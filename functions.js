const dbService = new DatabaseRequester(new FirebaseParser());
const renderer = new Renderer();

$(function() {
    const getListUseCase = new GetListUseCase(dbService, renderer);
    getListUseCase.doRequest();

    $("#search").on("input", function() {
        const query = $(this)[0].value;
        const res = getListUseCase.filter(query);
    });

    $("#cmdAdd").click(function() {
        const addItemUseCase = new AddItemUseCase(dbService);
        addItemUseCase.addRecord();
    });

    $(".code-snippet-impl").click(copySnippetToClipboard);
});

function copySnippetToClipboard(ev) {
    const cb = navigator.clipboard;
    const implString = this.innerText;
    cb.writeText(implString).then(() => renderer.showCopiedIcon(ev));
};