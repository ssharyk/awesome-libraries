class DatabaseRequester {

    constructor(parser) {
        this._parser = parser;

        this._groups = {};

        this._database = firebase.database();
    }

    addLibrary(groupName, library, onSuccess, onError) {
        if (groupName in this._groups) {

            this._groups[groupName].libraries = [...(this._groups[groupName].libraries || []), library]
        } else {
            this._groups[groupName] = new Group(groupName, [library]);
        }
        this._database.ref(`groups/${groupName}`).set(this._groups[groupName]);
    }

    getLibrariesList(onSuccess, onError) {
        this._database.ref('groups/').on('value', (snapshot) => {
            const data = snapshot.val();
            if (data) {
                this._groups = data;
                onSuccess(this._parser.parse(data));
            } else {
                onError(new Error("no data"));
            }
        });
    }

}