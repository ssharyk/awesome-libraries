class FirebaseParser {

    parse(data) {
        let groups = [];
        for (var groupName in data) {
            const item = data[groupName];
            const libs = (item.libraries || []).map(it => Library.from(it));
            const group = new Group(item.id, groupName, libs);
            groups.push(group);
        }
        return groups;
    }
}