class Renderer {

    /// TODO: as MVI pattern

    renderLibraries(groupsWithLibs) {
        /// TODO: clear all at start
        this._clearAll();

        var count = 0
        for (var groupIndex = 0; groupIndex < groupsWithLibs.length; groupIndex++) {
            const group = groupsWithLibs[groupIndex];
            const libs = group.libraries;

            this._renderHeader(group);
            for (var i = 0; i < libs.length; i++) {
                this._renderLibrary(group, libs[i]);
                count++;
            }
            this._renderFooter();
        }
        $(".code-snippet-impl").click(copySnippetToClipboard);
        return count;
    }

    renderSearchResults(groupsWithLibs) {
        const count = this.renderLibraries(groupsWithLibs);
        this._renderSearchResultsCount(count);
    }

    _clearAll() {
        $("#lstLibraries")[0].innerHTML = "";
    }

    _renderSearchResultsCount(count) {
        $("#txtCountResult")[0].innerHTML = count;
    }

    _renderHeader(group) {
        const headerHtml = `
        <li id="group-${group.id}">
            <h3 class="list-item-group-name">${group.name}</h3>
            <hr />
        `;
        $("#lstLibraries").append(headerHtml);
    }

    _renderLibrary(group, lib) {
        const libBodyHtml = `
        <div class="container list-item-library-container">
            <h5 class="list-item-library-name">${lib.name}</h5>
            <p>Artifact ID: ${lib.artifactId}</p>
            <p>
                <code class="code-snippet-impl">
                    ${lib.implementationString.replaceAll("\n", "<br />")}
                </code>
            </p>

            <div class="list-item-tags">
                <div class="list-item-tag">Last update: ${this._formatDate(lib.lastUpdate)}</div>
            </div>
        </div>

        <br />
        `;

        $(`#lstLibraries li#group-${group.id}`).append(libBodyHtml);
    }

    _renderFooter() {
        const footerHtml = "</li>";
        $("#lstLibraries").append(footerHtml);
    }

    _formatDate(timestamp) {
        return (new Date(timestamp)).toDateString();
    }


    // region Done button

    /**
     * Displays copy button in click position
     */
    showCopiedIcon(ev) {
        let elem = this._createCopyIconNode(ev.pageX, ev.pageY);
        document.body.appendChild(elem);

        setTimeout(() => {
            document.body.removeChild(elem);
        }, 1000);
    }

    /**
     * Adds new HTML tag for the single time
     */
    _createCopyIconNode(x, y) {
        let button = document.createElement('button');
        button.className = "btn btn-default";
        button.style.cssText = `
            position:absolute;
            left:${x}px;
            top:${y}px;

            opacity:0.85;
            z-index:10000;

            padding: 15px;
            border: 1px solid #d6e9c6;
            border-radius: 4px;
            color: #3c763d;
            background-color: #dff0d8;`;
        button.innerHTML = "Copied!";
        button.addEventListener('click', this._removeCopyIcon);
        return button;
    }

    // endregion

}