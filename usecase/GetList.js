class GetListUseCase {

    constructor(db, output) {
        this._db = db;
        this._renderer = output;
    }

    doRequest() {
        const errorHandler = function onError(err) {
            console.error(err);
        };

        const librariesHandler = (data) => {
            this._data = data;
            this._renderer.renderLibraries(data);
        };

        this._db.getLibrariesList(librariesHandler, errorHandler);
    }

    filter(queryOrigin) {
        const query = queryOrigin.toLowerCase();
        const filteredItems = [];
        for (var i = 0; i < this._data.length; i++) {
            const group = this._data[i];
            if (group.name.toLowerCase().indexOf(query) !== -1) {
                // in group name, add it and all it's libraries
                filteredItems.push(group);
                continue;
            }

            // check for specific libs in the group
            const filteredLibs = group.libraries.filter(l => 
                l.name.toLowerCase().indexOf(query) !== -1 ||
                l.artifactId.toLowerCase().indexOf(query) !== -1
            );
            if (filteredLibs && filteredLibs.length) {
                filteredItems.push(new Group(group.id, group.name, filteredLibs));
            }
        }

        this._renderer.renderSearchResults(filteredItems);
        return filteredItems;
    }

}