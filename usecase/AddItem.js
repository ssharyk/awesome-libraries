class AddItemUseCase {

    constructor(db) {
        this._db = db;
    }

    addRecord() {
        const groupName = this._getValue("txtGroupName");
        const libName = this._getValue("txtName");
        const libArtifactId = this._getValue("txtArtifactId");
        const libImplementationString = this._getValue("txtImplementationString");
        const library = new Library(libName, libArtifactId, null, 0, libImplementationString);

        this._db.addLibrary(groupName, library);
    }

    _getValue(id) {
        return $("#" + id)[0].value;
    }

}