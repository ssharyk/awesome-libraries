class Group {
    constructor(id, name, libs) {
        this.id = id;
        this.name = name;
        this.libraries = libs;
    }
}