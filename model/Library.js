class Library {
    constructor(name, artifactId, latestVersion, lastUpdate, implString) {
        this.name = name;
        this.artifactId = artifactId;
        this.latestVersion = latestVersion;
        this.lastUpdate = lastUpdate;
        this.implementationString = implString;
    }

    static from(jObj) {
        return new Library(
            jObj.name,
            jObj.artifactId,
            jObj.latestVersion,
            jObj.lastUpdate,
            jObj.implementationString
        );
    }
}